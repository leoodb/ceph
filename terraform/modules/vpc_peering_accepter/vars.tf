variable "peering_connection_id"{
  default = ""
}

variable "peer_name" {
  default = ""
}

variable "auto_accept" {
  default = "true"
}

variable "route_table_id" {
  default = ""
}

variable "destination_cidr" {
  default = ""
}
