resource "aws_vpc_peering_connection_accepter" "peer" {
  vpc_peering_connection_id = "${var.peering_connection_id}"
  auto_accept = "${var.auto_accept}"
tags {
  Side = "Accepter"
  Name = "${var.peer_name}"
  }
}

resource "aws_route" "accepter" {
  route_table_id = "${var.route_table_id}"
  destination_cidr_block = "${var.destination_cidr}"
  vpc_peering_connection_id = "${var.peering_connection_id}"
}
