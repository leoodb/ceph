variable "domain_name" {
  description = "Domain name for Elasticsearch cluster"
  default = "es-domain"
}

variable "elasticsearch_version" {
  description = "Version of Elasticsearch to deploy"
  default = "6.2"
}

variable "instance_type" {
  description = "ES instance type for data nodes in the cluster"
  default = "m4.large.elasticsearch"
}

variable "instance_count" {
  description = "Number of data nodes in the cluster"
  default     = 6
}

variable "dedicated_master_type" {
  description = "ES instance type to be used for dedicated masters"
  default     = "m4.large.elasticsearch"
}

variable "automated_snapshot_start_hour" {
  description = "Hour at which automated snapshots are taken (UTC)"
  default = 23
}

variable "es_zone_awareness" {
  description = "Enable/disable zone awareness"
  default     = true
}

variable "environment" {
  default = "dev"
}

variable "encrypt_at_rest" {
  description = "Enable/disable data encryption at rest"
  default = true
}

variable "ebs_volume_size" {
  description = "Specify EBS volume size"
  default     = 80
}

variable "ebs_volume_type" {
  description = "Storage type of EBS volumes"
  default     = "gp2"
}

variable "security_group_ids" {
  type = "list"
  default     = [""]
}

variable "subnet_ids" {
  type = "list"
  default     = [""]
}
