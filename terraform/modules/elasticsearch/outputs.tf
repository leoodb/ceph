output "arn" {
  description = "Domain Amazon Resource Name (ARN)"
  value       = "${aws_elasticsearch_domain.es_domain.arn}"
}

output "domain_id" {
  description = "Domain identifier"
  value       = "${aws_elasticsearch_domain.es_domain.domain_id}"
}

output "endpoint" {
  description = "Domain endpoint"
  value       = "${aws_elasticsearch_domain.es_domain.endpoint}"
}
