resource "aws_elasticsearch_domain" "es_domain" {
  domain_name           = "aws-es-${var.domain_name}"
  elasticsearch_version = "${var.elasticsearch_version}"

  encrypt_at_rest {
    enabled = "${var.encrypt_at_rest}"
  }

  cluster_config {
    instance_type            = "${var.instance_type}"
    instance_count           = "${var.instance_count}"
    dedicated_master_enabled = "${var.instance_count >= 10 ? true : false}"
    dedicated_master_count   = "${var.instance_count >= 10 ? 3 : 0}"
    dedicated_master_type    = "${var.instance_count >= 10 ? (var.dedicated_master_type != "false" ? var.dedicated_master_type : var.instance_type) : ""}"
    zone_awareness_enabled   = "${var.es_zone_awareness}"
  }

  vpc_options {
      security_group_ids = ["${var.security_group_ids}"]
      subnet_ids         = ["${var.subnet_ids}"]
  }

  advanced_options {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  ebs_options {
    ebs_enabled = true
    volume_size = "${var.ebs_volume_size}"
    volume_type = "${var.ebs_volume_type}"
  }

  snapshot_options {
    automated_snapshot_start_hour = "${var.automated_snapshot_start_hour}"
  }

  tags {
    Domain      = "${var.domain_name}"
    Environment = "${var.environment}"
  }
}

resource "aws_elasticsearch_domain_policy" "main" {
  domain_name = "${aws_elasticsearch_domain.es_domain.domain_name}"

  access_policies = <<POLICIES
{
    "Version": "2012-10-17",
    "Statement": [
        {
          "Effect": "Allow",
          "Principal": {
            "AWS": ["*"]
          },
          "Action": ["es:*"],
            "Resource": "${aws_elasticsearch_domain.es_domain.arn}/*"
        }
    ]
}
POLICIES
}
