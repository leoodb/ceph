resource "aws_elasticache_subnet_group" "generic_sgn" {
  name                 = "sgn-${var.environment}-${var.app_name}"
  subnet_ids           = ["${split(",", var.private_subnet_ids)}"]
}

resource "aws_elasticache_parameter_group" "elasticache_pg" {
  name   = "pg-${var.environment}-${var.app_name}-elasticache"
  family = "redis3.2"

  parameter {
    name  = "activerehashing"
    value = "yes"
  }
  parameter {
    name  = "min-slaves-to-write"
    value = "0"
  }
  parameter {
    name  = "cluster-enabled"
    value = "yes"
  }
}

resource "aws_security_group" "elasticache_security_group" {
  name   = "aws-${var.environment}-${var.app_name}-elc-sg"
  vpc_id = "${var.vpc_id}"
}

resource "aws_elasticache_replication_group" "redis_generic" {
  replication_group_id          = "${var.environment}-${var.app_name}"
  replication_group_description = "replication-${var.environment}-${var.app_name}"
  node_type                     = "${var.node_type}"
  number_cache_clusters         = "${var.num_members}"
  port                          = "${var.port_node}"
  engine                        = "${var.engine}"
  engine_version                = "${var.engine_version}"
  parameter_group_name          = "${aws_elasticache_parameter_group.elasticache_pg.name}"
  subnet_group_name             = "${aws_elasticache_subnet_group.generic_sgn.name}"
  automatic_failover_enabled    = true
  apply_immediately             = true
  security_group_ids            = ["${aws_security_group.elasticache_security_group.id}"]
  tags {
    Name  = "${var.app_name}-${var.app_name}"
  }
}
