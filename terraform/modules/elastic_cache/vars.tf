variable "engine" {
  default = "redis"
}

variable "private_subnet_ids" {
  default = "subnet-3e14fb75,subnet-5d845f39"
}

variable "node_type" {
  default = "cache.m3.medium"
}

variable "environment" {
  default = "hmg"
}

variable "port_node" {
  default = "6379"
}

variable "engine_version" {
  default = "3.2.10"
}

variable "vpc_id" {
  default = "vpc-a7aaa3de"
}

variable "num_members" {
  default = "2"
}

variable "app_name" {
  default = "replicationteste"
}
