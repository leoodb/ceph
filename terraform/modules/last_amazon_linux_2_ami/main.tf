data "aws_ami" "amazon_linux_2" {
    filter {
        name = "name"
        values = ["amzn2-ami-hvm*"]
    }
    most_recent = true
}
