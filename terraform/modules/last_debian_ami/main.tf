data "aws_ami" "debian" {
    filter {
        name = "name"
        values = ["debian-stretch-hvm-x86_64*"]
    }
    most_recent = true
}
