output "vpc" {
    value = "${data.aws_vpc.vpc.id}"
}

output "subnet" {
    value = "${data.aws_subnet.subnet.id}"
}
