data "aws_vpc" "vpc" {
    filter {
        name = "tag-key"
        values = ["Name"]
    }

    filter {
        name = "tag-value"
        values = ["*${var.ENV}*"]
    }
}

data "aws_subnet" "subnet" {
    vpc_id = "${data.aws_vpc.vpc.id}"
    
    filter {
        name = "tag:Name"
        values = ["*${var.ENV}*"]
    }

    filter {
        name = "tag:Name"
        values = ["*${var.ZONE}*"]
    }
}
