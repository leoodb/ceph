variable "peer_region" {
  default = "us-east-1"
}

variable "vpc_requester_id" {
  default = "vpc-9690f8ed"
}

variable "vpc_accepter_id" {
  default = "vpc-6cd0ed17"
}

variable "accepter_account_id" {
  default = "454080017722"
}

variable "auto_accept" {
  default = false
}

variable "route_table_id" {
  default = "rtb-c91e7cb5"
}

variable "destination_cidr" {
  default = "10.30.0.0/16"
}

variable "peering_connection_id" {
  default = "pcx-94bd65fc"
}
