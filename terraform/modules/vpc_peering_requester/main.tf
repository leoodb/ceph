resource "aws_vpc_peering_connection" "peer" {
  vpc_id = "${var.vpc_requester_id}"
  peer_vpc_id = "${var.vpc_accepter_id}"
  peer_owner_id = "${var.accepter_account_id}"
  peer_region = "${var.peer_region}"
  auto_accept = "${var.auto_accept}"
  tags {
    Side = "Requester"
  }
}

resource "aws_route" "requester" {
  route_table_id = "${var.route_table_id}"
  destination_cidr_block = "${var.destination_cidr}"
  vpc_peering_connection_id = "${var.peering_connection_id}"
}
