data "aws_vpc" "vpc" {
    filter {
        name = "tag-key"
        values = ["Name"]
    }

    filter {
        name = "tag-value"
        values = ["*${var.ENV}*"]
    }
}

data "aws_security_group" "default" {
    vpc_id = "${data.aws_vpc.vpc.id}"

    filter {
        name = "group-name"
        values = ["default"]
    }
}
