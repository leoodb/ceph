variable "app_name" {}
variable "key_pair" {}
variable "vpc_id" {}

variable "instance_type" {
  default = "t2.small"
}

variable "user_data_file" {
  default = "user_data.tpl"
}

variable "environment" {
  default = "dev"
}

variable "asg_minsize" {
  default = 2
}

variable "asg_maxsize" {
  default = 2
}

variable "aws_amis" {
  type = "map"

  default = {
    "ap-northeast-1" = "ami-25bd2743"
    "ap-northeast-2" = "ami-7248e81c"
    "ap-south-1"     = "ami-5d99ce32"
    "ap-southeast-1" = "ami-d2fa88ae"
    "ap-southeast-2" = "ami-b6bb47d4"
    "ca-central-1"   = "ami-dcad28b8"
    "eu-central-1"   = "ami-337be65c"
    "eu-west-1"      = "ami-6e28b517"
    "eu-west-2"      = "ami-ee6a718a"
    "eu-west-3"      = "ami-bfff49c2"
    "sa-east-1"      = "ami-f9adef95"
    "us-east-1"      = "ami-4bf3d731"
    "us-east-2"      = "ami-e1496384"
    "us-west-1"      = "ami-65e0e305"
    "us-west-2"      = "ami-a042f4d8"
  }
}

variable "region" {
  default = "us-east-1"
}

variable "default_security_groups" {
  type = "list"
}

variable "backup" {
  default = "yes"
}

variable "lc_public_ip" {
  default = false
}

variable "elb_subnets" {
  type = "list"
}

variable "elb_internal" {
  default = false
}

variable "asg_subnets" {
  type = "list"
}
