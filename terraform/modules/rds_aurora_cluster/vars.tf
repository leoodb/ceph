variable "vpc_id" {}

variable "rds_subnet_ids" {
  default = ""
}

variable "rds_password" {
  default = ""
}

variable "environment" {
  default = "dev"
}

variable "app_name" {
  default = "app"
}

variable "rds_engine" {
  default = "aurora-mysql"
}


variable "rds_instance_type" {
  default = "db.t2.micro"
}


variable "rds_skip_final_snapshot" {
  default = "False"
}

variable "rds_publicly_accessible" {
  default = "False"
}


variable "rds_master_username" {
  default = "dddaaaaa"
}

variable "database_name" {
  default = "aasddasda"
}

variable "engine_version" {
  default = "5.7"
}

variable "family_pg" {
  default = "aurora-mysql5.7"
}
