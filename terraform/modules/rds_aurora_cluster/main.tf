resource "random_string" "password" {
  length  = 16
  special = false
}

resource "aws_db_subnet_group" "generic_sgn" {
  name       = "sgn-${var.environment}-${var.app_name}-${var.rds_engine}"
  subnet_ids = ["${split(",",var.rds_subnet_ids)}"]

  tags {
    Name = "sgn-${var.environment}-${var.app_name}"
  }
}

resource "aws_rds_cluster_parameter_group" "pg_aurora" {
  name        = "pg-${var.environment}-${var.app_name}-${var.rds_engine}"
  family      = "${var.family_pg}"
  description = "RDS default cluster parameter group"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}


resource "aws_security_group" "rds_security_group" {
  name   = "aws-${var.environment}-${var.app_name}-rds-sg"
  vpc_id = "${var.vpc_id}"
}


resource "aws_rds_cluster" "aurora_cluster" {

    cluster_identifier               = "${var.environment}-${var.app_name}-cluster"
    database_name                    = "${var.database_name}"
    master_username                  = "${var.rds_master_username}"
    master_password                  = "${random_string.password.result}"
    master_password                  = "${var.rds_password == "" ?  random_string.password.result : var.rds_password}"
    engine                           = "${var.rds_engine}"
    engine_version                   = "${var.engine_version}"
    db_subnet_group_name             = "${aws_db_subnet_group.generic_sgn.id}"
    skip_final_snapshot              = "${var.rds_skip_final_snapshot}"
    final_snapshot_identifier        = "${var.app_name}-final-snapshot-${md5(timestamp())}"
    db_cluster_parameter_group_name  = "${aws_rds_cluster_parameter_group.pg_aurora.name}"
    apply_immediately             = true

    vpc_security_group_ids        = [
        "${aws_security_group.rds_security_group.id}"
    ]

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_rds_cluster_instance" "aurora_cluster_instance" {

    count                   = "${length(split(",", var.rds_subnet_ids))}"

    identifier              = "${var.environment}-${var.app_name}-db${count.index}"
    cluster_identifier      = "${aws_rds_cluster.aurora_cluster.id}"
    engine                  = "${var.rds_engine}"
    engine_version          = "${var.engine_version}"
    instance_class          = "${var.rds_instance_type}"
    db_subnet_group_name    = "${aws_db_subnet_group.generic_sgn.id}"
    publicly_accessible     = "${var.rds_publicly_accessible}"

    lifecycle {
        create_before_destroy = true
    }

}
