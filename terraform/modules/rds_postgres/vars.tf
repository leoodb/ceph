variable "vpc_id" {}

variable "rds_subnet_ids" {
  type = "list"
}

variable "rds_password" {
  default = ""
}

variable "environment" {
  default = "dev"
}

variable "app_name" {
  default = "app"
}

variable "db_name" {
  default = ""
}

variable "rds_subnet_group_name" {
  default = "vpc-dev"
}

variable "rds_diskspace" {
  default = "10"
}

variable "rds_engine" {
  default = "postgres"
}

variable "rds_engine_version" {
  default = "9.6"
}

variable "rds_instance_type" {
  default = "db.t2.micro"
}

variable "rds_multi_az" {
  default = true
}

variable "rds_parameter_group" {
  default = "postgres9.6"
}

variable "rds_skip_final_snapshot" {
  default = "False"
}

variable "rds_publicly_accessible" {
  default = "False"
}

variable "rds_security_groups" {
  type    = "list"
  default = []
}
