output "rds_password" {
  value = "${var.rds_password == "" ?  random_string.password.result : var.rds_password}"
}

output "rds_username" {
  value = "${aws_db_instance.rds_generic.username}"
}

output "rds_endpoint" {
  value = "${aws_db_instance.rds_generic.endpoint}"
}

output "rds_address" {
  value = "${aws_db_instance.rds_generic.address}"
}
