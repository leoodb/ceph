data "aws_ami" "amazon_linux" {
    filter {
        name = "name"
        values = ["amzn-ami-hvm*"]
    }
    most_recent = true
}
