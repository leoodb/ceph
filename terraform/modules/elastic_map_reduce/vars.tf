variable "app_name" {
  default = "spk01dev"
}

variable "environment" {
  default = "dev"
}

variable "applications" {
  default = "Spark"
}

variable "release_label" {
  default = "emr-5.12.0"
}

variable "termination_protection" {
  default = "false"
}

variable "keep_job" {
  default = "true"
}

variable "vpc_id" {
  default = "vpc-0cd2a7e8f835c2ec2"
}

variable "subnet_id" {
  default = "subnet-0cad1098acf3199eb"
}

variable "instance_type_core" {
  default = "m5.xlarge"
}

variable "instance_number_cores" {
  default = "3"
}

variable "instance_type_master" {
  default = "m5.xlarge"
}

variable "ebs_volume" {
  default = "30"
}

variable "bucket_s3" {
  default = "s3://spk01dev-logs/"
}

variable "key_name" {
  default = "key-spk01dev"
}


variable "protocol" {
  default = "tcp"
}

variable "from_port" {
  default = "0"
}

variable "to_port" {
  default = "65535"
}

variable "range" {
  default = "172.16.0.0/12,10.8.0.0/16"
}
