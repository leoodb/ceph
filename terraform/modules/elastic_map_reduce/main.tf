resource "aws_iam_role" "iam_emr_service_role" {
  name = "aws-${var.environment}-${var.app_name}-emr-service-role"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "elasticmapreduce.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "iam_emr_service_policy" {
  name = "aws-${var.environment}-${var.app_name}-emr-serive-policy"
  role = "${aws_iam_role.iam_emr_service_role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [{
        "Effect": "Allow",
        "Resource": "*",
        "Action": [
          "ec2:AuthorizeSecurityGroupEgress",
          "ec2:AuthorizeSecurityGroupIngress",
          "ec2:CancelSpotInstanceRequests",
          "ec2:CreateNetworkInterface",
          "ec2:CreateSecurityGroup",
          "ec2:CreateTags",
          "ec2:DeleteNetworkInterface",
          "ec2:DeleteSecurityGroup",
          "ec2:DeleteTags",
          "ec2:DescribeAvailabilityZones",
          "ec2:DescribeAccountAttributes",
          "ec2:DescribeDhcpOptions",
          "ec2:DescribeImages",
          "ec2:DescribeInstanceStatus",
          "ec2:DescribeInstances",
          "ec2:DescribeKeyPairs",
          "ec2:DescribeNetworkAcls",
          "ec2:DescribeNetworkInterfaces",
          "ec2:DescribePrefixLists",
          "ec2:DescribeRouteTables",
          "ec2:DescribeSecurityGroups",
          "ec2:DescribeSpotInstanceRequests",
          "ec2:DescribeSpotPriceHistory",
          "ec2:DescribeSubnets",
          "ec2:DescribeTags",
          "ec2:DescribeVpcAttribute",
          "ec2:DescribeVpcEndpoints",
          "ec2:DescribeVpcEndpointServices",
          "ec2:DescribeVpcs",
          "ec2:DetachNetworkInterface",
          "ec2:ModifyImageAttribute",
          "ec2:ModifyInstanceAttribute",
          "ec2:RequestSpotInstances",
          "ec2:RevokeSecurityGroupEgress",
          "ec2:RunInstances",
          "ec2:TerminateInstances",
          "ec2:DeleteVolume",
          "ec2:DescribeVolumeStatus",
          "ec2:DescribeVolumes",
          "ec2:DetachVolume",
          "iam:GetRole",
          "iam:GetRolePolicy",
          "iam:ListInstanceProfiles",
          "iam:ListRolePolicies",
          "iam:PassRole",
          "s3:CreateBucket",
          "s3:Get*",
          "s3:List*",
          "sdb:BatchPutAttributes",
          "sdb:Select",
          "sqs:CreateQueue",
          "sqs:Delete*",
          "sqs:GetQueue*",
          "sqs:PurgeQueue",
          "sqs:ReceiveMessage",
          "cloudwatch:PutMetricAlarm",
          "cloudwatch:DescribeAlarms",
          "cloudwatch:DeleteAlarms",
          "application-autoscaling:RegisterScalableTarget",
          "application-autoscaling:DeregisterScalableTarget",
          "application-autoscaling:PutScalingPolicy",
          "application-autoscaling:DeleteScalingPolicy",
          "application-autoscaling:Describe*"
        ]
    }]
}
EOF
}

resource "aws_iam_role" "iam_emr_profile_role" {
  name = "aws-${var.environment}-${var.app_name}-emr-profile-role"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "emr_profile" {
  name = "aws-${var.environment}-${var.app_name}-emr-profile"
  role = "${aws_iam_role.iam_emr_profile_role.name}"
}

resource "aws_iam_role_policy" "iam_emr_profile_policy" {
  name = "aws-${var.environment}-${var.app_name}-emr-profile-policy"
  role = "${aws_iam_role.iam_emr_profile_role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [{
        "Effect": "Allow",
        "Resource": "*",
        "Action": [
          "cloudwatch:*",
          "dynamodb:*",
          "ec2:Describe*",
          "elasticmapreduce:Describe*",
          "elasticmapreduce:ListBootstrapActions",
          "elasticmapreduce:ListClusters",
          "elasticmapreduce:ListInstanceGroups",
          "elasticmapreduce:ListInstances",
          "elasticmapreduce:ListSteps",
          "kinesis:CreateStream",
          "kinesis:DeleteStream",
          "kinesis:DescribeStream",
          "kinesis:GetRecords",
          "kinesis:GetShardIterator",
          "kinesis:MergeShards",
          "kinesis:PutRecord",
          "kinesis:SplitShard",
          "rds:Describe*",
          "s3:*",
          "sdb:*",
          "sns:*",
          "sqs:*",
          "glue:CreateDatabase",
          "glue:UpdateDatabase",
          "glue:DeleteDatabase",
          "glue:GetDatabase",
          "glue:GetDatabases",
          "glue:CreateTable",
          "glue:UpdateTable",
          "glue:DeleteTable",
          "glue:GetTable",
          "glue:GetTables",
          "glue:GetTableVersions",
          "glue:CreatePartition",
          "glue:BatchCreatePartition",
          "glue:UpdatePartition",
          "glue:DeletePartition",
          "glue:BatchDeletePartition",
          "glue:GetPartition",
          "glue:GetPartitions",
          "glue:BatchGetPartition",
          "glue:CreateUserDefinedFunction",
          "glue:UpdateUserDefinedFunction",
          "glue:DeleteUserDefinedFunction",
          "glue:GetUserDefinedFunction",
          "glue:GetUserDefinedFunctions"
        ]
    }]
}
EOF
}

resource "aws_security_group" "elastic_map_reduce_security_group" {
  name   = "aws-${var.environment}-${var.app_name}-emr-sg"
  vpc_id = "${var.vpc_id}"
  revoke_rules_on_delete = "true"

  ingress {
    protocol    = "${var.protocol}"
    from_port   = "${var.from_port}"
    to_port     = "${var.to_port}"
    cidr_blocks = ["${split(",", var.range)}"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "elastic_map_reduce_security_group_access" {
  name   = "aws-${var.environment}-${var.app_name}-emr-sg-acess"
  vpc_id = "${var.vpc_id}"
  revoke_rules_on_delete = "true"

  ingress {
    protocol    = "${var.protocol}"
    from_port   = "${var.from_port}"
    to_port     = "${var.to_port}"
    cidr_blocks = ["${split(",", var.range)}"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_emr_cluster" "emr-template-cluster" {
  name          = "${var.app_name}"
  release_label = "${var.release_label}"
  applications  = ["${split(",",var.applications)}"]

  termination_protection            = "${var.termination_protection}"
  keep_job_flow_alive_when_no_steps = "${var.keep_job}"

  ec2_attributes {
    subnet_id                         = "${var.subnet_id}"
    emr_managed_master_security_group = "${aws_security_group.elastic_map_reduce_security_group.id}"
    emr_managed_slave_security_group  = "${aws_security_group.elastic_map_reduce_security_group.id}"
    service_access_security_group     = "${aws_security_group.elastic_map_reduce_security_group_access.id}"
    instance_profile                  = "${aws_iam_instance_profile.emr_profile.arn}"
    key_name                          = "${var.key_name}"
  }

  ebs_root_volume_size = "${var.ebs_volume}"

  master_instance_type = "${var.instance_type_master}"
  core_instance_type   = "${var.instance_type_core}"
  core_instance_count  = "${var.instance_number_cores}"

  tags {
    app         = "${var.app_name}"
    env         = "${var.environment}"
  }

  log_uri      = "${var.bucket_s3}"
  service_role = "${aws_iam_role.iam_emr_service_role.arn}"
}
